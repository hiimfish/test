def build(String tag) {
    def scriptcontents = libraryResource "Dockerfile"
    writeFile file:"Dockerfile", text: scriptcontents

    docker.withRegistry('https://126802494159.dkr.ecr.ap-northeast-2.amazonaws.com', 'ecr:ap-northeast-2:9d934516-0adf-4413-8f7e-4369ebc7daab') {
        def customImage = docker.build("jn:jenkins-dev")

        customImage.push()
    }
}
